import { CircularProgress, Box, TextField, Pagination } from "@mui/material";
import { RouteComponentProps } from "@reach/router";
import React, { FC, useEffect, useMemo, useState } from "react";
import { BackendClient } from "../clients/backend.client";
import { UserCard } from "../components/users/user-card";
import { IUserProps } from "../dtos/user.dto";


const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState('');
  const [page, setPage] = useState(1);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      const result = await backendClient.getAllUsers(page * 10, 10);
      setUsers(result.data);
      setLoading(false);
    };

    fetchData();
  }, [page]);

  const memoizedUsers = useMemo(() => {
    if(!search) {
      return users
    }

    return users.filter((user) => user.title?.toLowerCase().includes(search) || user.gender?.toLowerCase().includes(search) || user.company?.toLowerCase().includes(search))
  }, [search, users])

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {loading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            <Box>
              <TextField value={search} onChange={e => setSearch(e.target.value)}/>
            </Box>
            {memoizedUsers.length
              ? memoizedUsers.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
              <Pagination count={10} onChange={(e, num) => setPage(num)} />
          </div>
        )}
      </div>
    </div>
  );
};
